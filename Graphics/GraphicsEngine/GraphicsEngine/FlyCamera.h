#pragma once
#include "Camera.h"

/**********************************************************************************************//**
 * \class	FlyCamera
 *
 * \brief	A fly camera.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \sa	Camera	
 **************************************************************************************************/

class FlyCamera : public Camera
{
public:

	/**********************************************************************************************//**
	 * \fn	FlyCamera::FlyCamera();
	 *
	 * \brief	Initializes a new instance of the <see cref="FlyCamera"/> class.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	FlyCamera();

	/**********************************************************************************************//**
	 * \fn	FlyCamera::~FlyCamera();
	 *
	 * \brief	Finalizes an instance of the <see cref="FlyCamera"/> class.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	~FlyCamera();

	/**********************************************************************************************//**
	 * \fn	void FlyCamera::Update(float deltatime, GLFWwindow* window);
	 *
	 * \brief	Updates the specified deltatime.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param 		  	deltatime	The deltatime.
	 * \param [in,out]	window   	The window.
	 **************************************************************************************************/

	void Update(float deltatime, GLFWwindow* window);

	/**********************************************************************************************//**
	 * \fn	void FlyCamera::setSpeed(float speed);
	 *
	 * \brief	Sets the speed.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param	speed	The speed.
	 **************************************************************************************************/

	void setSpeed(float speed);

private:
	/** \brief	The m speed */
	float m_speed;
	/** \brief	The m up */
	glm::vec3 m_up;

	/** \brief	The m d mouse x */
	double m_dMouseX;
	/** \brief	The m d mouse y */
	double m_dMouseY;

	/** \brief	The m d delta mouse x */
	double m_dDeltaMouseX;
	/** \brief	The m d delta mouse y */
	double m_dDeltaMouseY;
};

