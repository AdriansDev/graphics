#include "Application.h"
#include <iostream>
#include <chrono>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "../Dep/glCore/gl_core_4_5.h"
#include <glfw3.h>

#include "Gizmos.h"

/**********************************************************************************************//**
 * \fn	int main()
 *
 * \brief	Mains this instance.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \return	Exit-code for the process - 0 for success, else an error code.
 **************************************************************************************************/

int main()
{
	Application App;

	/**********************************************************************************************//**
	 * \fn	App.Initialize();
	 *
	 * \brief	Default constructor
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	App.Initialize();

	/**********************************************************************************************//**
	 * \fn	App.Run();
	 *
	 * \brief	Default constructor
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	App.Run();

	/**********************************************************************************************//**
	 * \fn	App.Terminate();
	 *
	 * \brief	Default constructor
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	App.Terminate();

	return 0;
}
