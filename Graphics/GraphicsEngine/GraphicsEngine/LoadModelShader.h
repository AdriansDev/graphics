#pragma once
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <iostream>

#include "../Dep/glCore/gl_core_4_5.h"
#include <glfw3.h>

 
#include "Shader.h"
#include "OBJMesh.h"

/**********************************************************************************************//**
 * \class	LoadModelShader
 *
 * \brief	A load model shader.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

class LoadModelShader
{
public:

	/**********************************************************************************************//**
	 * \fn	LoadModelShader::LoadModelShader();
	 *
	 * \brief	Initializes a new instance of the <see cref="LoadModalShader" /> class.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	LoadModelShader();

	/**********************************************************************************************//**
	 * \fn	LoadModelShader::~LoadModelShader();
	 *
	 * \brief	Finalizes an instance of the <see cref="LoadModalShader" /> class.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	~LoadModelShader();

	/**********************************************************************************************//**
	 * \fn	int LoadModelShader::LoadShader(aie::ShaderProgram& shaderprogram, const char * vertFileLocation, const char * fragFileLocation);
	 *
	 * \brief	Loads the shader.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param [in,out]	shaderprogram   	The shaderprogram.
	 * \param 		  	vertFileLocation	The vert file location.
	 * \param 		  	fragFileLocation	The frag file location.
	 *
	 * \return	The shader.
	 **************************************************************************************************/

	int LoadShader(aie::ShaderProgram& shaderprogram, const char * vertFileLocation, const char * fragFileLocation);

	/**********************************************************************************************//**
	 * \fn	int LoadModelShader::LoadModel(const char* objectname, aie::OBJMesh& Mesh, const char * objFile);
	 *
	 * \brief	Loads the model.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param 		  	objectname	The objectname.
	 * \param [in,out]	Mesh	  	The mesh.
	 * \param 		  	objFile   	The object file.
	 *
	 * \return	The model.
	 **************************************************************************************************/

	int LoadModel(const char* objectname, aie::OBJMesh& Mesh, const char * objFile);
};

