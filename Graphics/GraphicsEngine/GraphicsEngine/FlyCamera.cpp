#include "FlyCamera.h"

/**********************************************************************************************//**
 * \fn	FlyCamera::FlyCamera()
 *
 * \brief	Default constructor
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

FlyCamera::FlyCamera() : Camera()
{
	// Sets defult speed
	m_speed = 20;
}

/**********************************************************************************************//**
 * \fn	FlyCamera::~FlyCamera()
 *
 * \brief	Destructor
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

FlyCamera::~FlyCamera()
{
}

/**********************************************************************************************//**
 * \fn	void FlyCamera::Update(float deltatime, GLFWwindow* window)
 *
 * \brief	Updates this object
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \param 		  	deltatime	The deltatime.
 * \param [in,out]	window   	If non-null, the window.
 **************************************************************************************************/

void FlyCamera::Update(float deltatime, GLFWwindow* window)
{
	//Calculate delta XY of mouse
	glfwGetCursorPos(window, &m_dMouseX, &m_dMouseY);

	////Radians to degress
	m_dDeltaMouseX = (720 - m_dMouseX) * 0.0174533;
	m_dDeltaMouseY = (450 - m_dMouseY) * 0.0174533;

	////Keep mouse locked to screen
	glfwSetCursorPos(window, 720, 450);

	////Calculate relative world up
	glm::vec4 up = glm::inverse(worldTransform) * glm::vec4(0, 1, 0, 0);
	glm::mat4 rotMat(1.0f);

	////Rotate around world up
	rotMat = glm::rotate((float)-m_dDeltaMouseX * deltatime, glm::vec3(up[0], up[1], up[2]));
	viewTransform = rotMat * viewTransform;

	////Rotate up down
	rotMat = glm::rotate((float)-m_dDeltaMouseY * deltatime, glm::vec3(1, 0, 0));
	viewTransform = rotMat * viewTransform;

	////Update world transform
	worldTransform = glm::inverse(viewTransform);

	
	//WASD Controls for fly camera
	if (glfwGetKey(window, GLFW_KEY_W))
	{
		worldTransform[3] += worldTransform[2] * deltatime * -m_speed;
	}
	if (glfwGetKey(window, GLFW_KEY_S))
	{
		worldTransform[3] += worldTransform[2] * deltatime * m_speed;
	}
	if (glfwGetKey(window, GLFW_KEY_A))
	{
		worldTransform[3] += worldTransform[0] * deltatime * -m_speed;
	}
	if (glfwGetKey(window, GLFW_KEY_D))
	{
		worldTransform[3] += worldTransform[0] * deltatime * m_speed;
	}

	worldTransform[3][3] = 1.0f;
	viewTransform = glm::inverse(worldTransform);
	updateProjectionViewTransform();
}

/**********************************************************************************************//**
 * \fn	void FlyCamera::setSpeed(float speed)
 *
 * \brief	Speed Variable Set
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \param	speed	The speed.
 **************************************************************************************************/

void FlyCamera::setSpeed(float speed)
{
	//Sets Speed
	speed = m_speed;
}
