#pragma once
#include "glm.hpp"
#include "../Dep/glCore/gl_core_4_5.h"

/**********************************************************************************************//**
 * \class	Mesh
 *
 * \brief	A mesh.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

class Mesh
{
public:

	/**********************************************************************************************//**
	 * \fn	Mesh::Mesh();
	 *
	 * \brief	Initializes a new instance of the <see cref="Mesh"/> class.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	Mesh();

	/**********************************************************************************************//**
	 * \fn	virtual Mesh::~Mesh();
	 *
	 * \brief	Finalizes an instance of the <see cref="Mesh" /> class.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	virtual ~Mesh();

	/**********************************************************************************************//**
	 * \struct	Vertex
	 *
	 * \brief	Vertex Struct
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	struct Vertex {
		glm::vec4 position;
		glm::vec4 normal;
		glm::vec2 texCoord;
	};

	/**********************************************************************************************//**
	 * \fn	void Mesh::initialise(unsigned int vertexCount, const Vertex* vertices, unsigned int indexCount = 0, unsigned int* indices = nullptr);
	 *
	 * \brief	Initialises this object
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param 		  	vertexCount	Number of vertices.
	 * \param 		  	vertices   	The vertices.
	 * \param 		  	indexCount 	(Optional) Number of indexes.
	 * \param [in,out]	indices	   	(Optional) If non-null, the indices.
	 **************************************************************************************************/

	void initialise(unsigned int vertexCount, const Vertex* vertices,
		unsigned int indexCount = 0,
		unsigned int* indices = nullptr);

	/**********************************************************************************************//**
	 * \fn	void Mesh::initialiseQuad();
	 *
	 * \brief	Initialises the quad.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	void initialiseQuad();

	/**********************************************************************************************//**
	 * \fn	virtual void Mesh::draw();
	 *
	 * \brief	Draws this instance.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	virtual void draw();

protected:

	/** \brief	The tri count */
	unsigned int triCount;

	/**********************************************************************************************//**
	 * \property	unsigned int vao, vbo, ibo
	 *
	 * \brief	the ibo, vao, vbo
	 *
	 **************************************************************************************************/

	unsigned int vao, vbo, ibo;



};

