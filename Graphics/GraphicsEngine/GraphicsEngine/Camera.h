#pragma once
#include <glm.hpp>
#include <ext.hpp>
#include <glfw3.h>

/**********************************************************************************************//**
 * \class	Camera
 *
 * \brief	A camera.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

class Camera
{
public:

	/**********************************************************************************************//**
	 * \fn	Camera::Camera();
	 *
	 * \brief	Initializes a new instance of the <see cref="Camera"/> class.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	Camera();

	/**********************************************************************************************//**
	 * \fn	Camera::~Camera();
	 *
	 * \brief	Finalizes an instance of the <see cref="Camera"/> class.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	~Camera();

	/**********************************************************************************************//**
	 * \fn	virtual void Camera::Update(float deltatime, GLFWwindow* window) = 0;
	 *
	 * \brief	Updates the specified deltatime.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param 		  	deltatime	The deltatime.
	 * \param [in,out]	window   	The window.
	 **************************************************************************************************/

	virtual void Update(float deltatime, GLFWwindow* window) = 0;

	/**********************************************************************************************//**
	 * \fn	void Camera::setPerspective(float fieldOfView, float aspectRatio, float near, float far);
	 *
	 * \brief	Sets the perspective.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param	fieldOfView	The field of view.
	 * \param	aspectRatio	The aspect ratio.
	 * \param	near	   	The near.
	 * \param	far		   	>The far.
	 **************************************************************************************************/

	void setPerspective(float fieldOfView, float aspectRatio, float near, float far);

	/**********************************************************************************************//**
	 * \fn	void Camera::setLookAt(glm::vec3 from, glm::vec3 to, glm::vec3 up);
	 *
	 * \brief	Sets the look at.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param	from	From.
	 * \param	to  	To.
	 * \param	up  	Up.
	 **************************************************************************************************/

	void setLookAt(glm::vec3 from, glm::vec3 to, glm::vec3 up);

	/**********************************************************************************************//**
	 * \fn	void Camera::setPosition(glm::vec3 position);
	 *
	 * \brief	Sets the position.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param	position	The position.
	 **************************************************************************************************/

	void setPosition(glm::vec3 position);

	/**********************************************************************************************//**
	 * \fn	glm::mat4 Camera::getWorldTransform();
	 *
	 * \brief	Gets the world transform.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \return	The world transform.
	 **************************************************************************************************/

	glm::mat4 getWorldTransform();

	/**********************************************************************************************//**
	 * \fn	glm::mat4 Camera::getView();
	 *
	 * \brief	Gets the view.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \return	The view.
	 **************************************************************************************************/

	glm::mat4 getView();

	/**********************************************************************************************//**
	 * \fn	glm::mat4 Camera::getProjection();
	 *
	 * \brief	Gets the projection.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \return	The projection.
	 **************************************************************************************************/

	glm::mat4 getProjection();

	/**********************************************************************************************//**
	 * \fn	glm::mat4 Camera::getProjectionView();
	 *
	 * \brief	Gets the projection view.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \return	The projection view.
	 **************************************************************************************************/

	glm::mat4 getProjectionView();

protected:

	/**********************************************************************************************//**
	 * \fn	void Camera::updateProjectionViewTransform();
	 *
	 * \brief	Updates the projection view transform.
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	void updateProjectionViewTransform();

	/** \brief	The world transform */
	glm::mat4 worldTransform;

	/** \brief	The view transform */
	glm::mat4 viewTransform;

	/** \brief	The projection transform */
	glm::mat4 projectionTransform;

	/** \brief	The view x projection */
	glm::mat4 projection_x_view;
};

