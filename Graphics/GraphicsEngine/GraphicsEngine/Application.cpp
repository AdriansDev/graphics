#include "Application.h"

/**********************************************************************************************//**
 * \fn	Application::Application()
 *
 * \brief	Default constructor
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

Application::Application()
{
}

/**********************************************************************************************//**
 * \fn	Application::~Application()
 *
 * \brief	Destructor
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

Application::~Application()
{
}

/**********************************************************************************************//**
 * \fn	int Application::Initialize(const glm::ivec2 & a_resolution, const char * a_name)
 *
 * \brief	Initializes this object
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \param	a_resolution	The resolution.
 * \param	a_name			The name.
 *
 * \return	An int.
 **************************************************************************************************/

int Application::Initialize(const glm::ivec2 & a_resolution, const char * a_name)
{
	//Creates new Fly Camera
	cam = new FlyCamera();
	
	//Clock Setup
	m_startTime = m_clock.now();
	m_currentTime = m_clock.now();
	m_previousTime = m_clock.now();

	// If we can hook into the GPU
	if (glfwInit() == false)
		return -1; // -1 is a failure code 

	//Make a window with openGL render   Width, Height, Window name, Screen pointer
	window = glfwCreateWindow(a_resolution.x,a_resolution.y, a_name, nullptr, nullptr);

	//Check window worked
	if (window == nullptr)
	{
		glfwTerminate();
		return -2;
	}

	int count = 0;
	screens = glfwGetMonitors(&count);

	//Bring to front
	glfwMakeContextCurrent(window);

	// before loading interface
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED) {
		glfwDestroyWindow(window);
		glfwTerminate();
		return -3;
	}

	//Open GL Version
	auto major = ogl_GetMajorVersion();
	auto minor = ogl_GetMinorVersion();
	printf("GL: %i.%i\n", major, minor);

	glClearColor(0.25f, 0.25f, 0.25f, 1);

	glEnable(GL_DEPTH_TEST);

	//AIE Gizmos
	aie::Gizmos::create(10000, 10000, 10000, 10000);

	//Sets cursor position to face centre of screen
	glfwSetCursorPos(window, 720, 450);

	//Sets look at for camera
	cam->setLookAt(glm::vec3(10, 10, 10), glm::vec3(0), glm::vec3(0, 1, 0));
	
	//Sets perspective for camera
	cam->setPerspective(0.25f, 16 / 9.f, 0.1f, 1000.f);


	//LIGHTS for spear
	//Lighting Diffuse
	m_light.diffuse = { 1, 1, 1 };
	m_light.specular = { 1, 1, 1 };
	m_ambientLight = { 0.25f, 0.25f, 0.25f };

	//LIGHTS for camera
	m_light2.diffuse = { 1, 1, 1 };
	m_light2.specular = { 1, 1, 1 };
	m_light2.direction = { 0, -1, 0 };

	//LIGHTS for Rifle
	m_light3.diffuse = { 1, 1, 1 };
	m_light3.specular = { 1, 1, 1 };
	m_light3.direction = { 0, 1, 0 };

	//Lights for Bunny
	m_light4.direction = { 0, -1, 0 };

	//Lights for Buddha
	m_light5.direction = { 0, -1, 0 };

	//Load phong shader
	LoadShaderandObject.LoadShader(m_phongShader, "../bin/shaders/phong.vert", "../bin/shaders/phong.frag");

	//LOAD BUNNY SHADER
	LoadShaderandObject.LoadShader(m_bunnyShader, "../bin/shaders/simple.vert", "../bin/shaders/simple.frag");

	//Load camera Shaders
	LoadShaderandObject.LoadShader(m_rifleShader, "../bin/shaders/normalmap.vert", "../bin/shaders/normalmap.frag");

	//Normal map shader
	LoadShaderandObject.LoadShader(m_shader, "../bin/shaders/normalmap.vert", "../bin/shaders/normalmap.frag");

	//Load Rifle mesh
	LoadShaderandObject.LoadModel("Rifle", m_rifleMesh, "../bin/rifle/Rifle.obj");

	//Transforms for Rifle
	m_rifleTransform = {
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		0,0,0,1
	};

	//Load bunny mesh
	LoadShaderandObject.LoadModel("Bunny", m_bunnyMesh, "../bin/stanford/Bunny.obj");

	//Transforms for bunny
	m_bunnyTransform = {
		0.5f,0,0,0,
		0,0.5f,0,0,
		0,0,0.5f,0,
		-5,0,-5,1
	};

	//Load buddha model
	LoadShaderandObject.LoadModel("Buddha", m_BuddhaMesh, "../bin/stanford/Buddha.obj");

	//Transforms for buddha
	m_BuddhaTransform = {
		0.5f,0,0,0,
		0,0.5f,0,0,
		0,0,0.5f,0,
		5,0,-5,1
	};


	//LOAD SPEAR
	LoadShaderandObject.LoadModel("Spear", m_spearMesh, "../bin/soulspear/soulspear.obj");

	//Transforms for spear
	m_spearTransform = {
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		-5,0,5,1
	};

	//Load camera Shaders
	LoadShaderandObject.LoadShader(m_cameraShader, "../bin/shaders/normalmap.vert", "../bin/shaders/normalmap.frag");

	//Load camera Object
	LoadShaderandObject.LoadModel("Camera", m_CameraMesh, "../bin/Camera/leica.obj");

	//Transforms for Camera
	m_CameraTransform = {
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		5,2,5,1
	};
	return 1;
}

/**********************************************************************************************//**
 * \fn	void Application::Run()
 *
 * \brief	Runs this object
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

void Application::Run()
{
	while (glfwWindowShouldClose(window) == false && glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		//Time Settings
		m_previousTime = m_currentTime;
		m_currentTime = m_clock.now();
		auto duration = m_currentTime - m_previousTime;
		deltatime = duration.count() * NANO_TO_SECONDS;

		cam->Update((float)deltatime, window);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// query time since application started
		double time = glfwGetTime();
		// rotate light
		m_light.direction = glm::normalize(glm::vec3(glm::cos(time * 2), 0,
			glm::sin(time * 2)));
	
		//Adds X, Y, Z Transform in middle of grid
		aie::Gizmos::addTransform(glm::mat4(1));

		//White and black colors set for grid
		glm::vec4 white(1);
		glm::vec4 black(0, 0, 0, 1);
		
		//For loop for grid
		for (int i = 0; i < 21; ++i) {

			//Adds lines for grid
			aie::Gizmos::addLine(glm::vec3(-10 + i, 0, 10), glm::vec3(-10 + i, 0, -10), i == 10 ? white : black);
			aie::Gizmos::addLine(glm::vec3(10, 0, -10 + i), glm::vec3(-10, 0, -10 + i), i == 10 ? white : black);
		}
		//Initialize renderer
		Render();
		glfwPollEvents();
	}
}

/**********************************************************************************************//**
 * \fn	void Application::Render()
 *
 * \brief	Renders this object
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

void Application::Render()
{
	// update perspective in case window resized
	cam->setPerspective(0.25f, 16 / 9.f, 0.1f, 1000.f);

	//Bind Spear Shader
	m_shader.bind();

	//PVM calc for Lighting
	auto pvm = cam->getProjection() * cam->getView() * m_spearTransform;
	m_shader.bindUniform("ProjectionViewModel", pvm);
	
	// Binds light Uniforms
	m_shader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_spearTransform)));
	m_shader.bindUniform("cameraPosition", glm::vec3(glm::inverse(cam->getView())[3]));
	m_shader.bindUniform("Ia", m_ambientLight);
	m_shader.bindUniform("Id", m_light.diffuse);
	m_shader.bindUniform("Is", m_light.specular);
	m_shader.bindUniform("lightDirection", m_light.direction);

	//Draw Spear mesh
	m_spearMesh.draw();

	// Binds light Uniforms
	m_shader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_CameraTransform)));
	m_shader.bindUniform("cameraPosition", glm::vec3(glm::inverse(cam->getView())[3]));
	m_shader.bindUniform("Ia", m_ambientLight);
	m_shader.bindUniform("Id", m_light2.diffuse);
	m_shader.bindUniform("Is", m_light2.specular);
	m_shader.bindUniform("lightDirection", m_light2.direction);

	//Pvm for Camera
	pvm = cam->getProjection() * cam->getView() * m_CameraTransform;
	m_cameraShader.bindUniform("ProjectionViewModel", pvm);

	//Camera Mesh Draw
	m_CameraMesh.draw();


	// Binds light Uniforms
	m_shader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_rifleTransform)));
	m_shader.bindUniform("cameraPosition", glm::vec3(glm::inverse(cam->getView())[3]));
	m_shader.bindUniform("Ia", m_ambientLight);
	m_shader.bindUniform("Id", m_light3.diffuse);
	m_shader.bindUniform("Is", m_light3.specular);
	m_shader.bindUniform("lightDirection", m_light3.direction);

	//Pvm for rifle
	pvm = cam->getProjection() * cam->getView() * m_rifleTransform;
	m_rifleShader.bindUniform("ProjectionViewModel", pvm);

	//Camera Mesh Draw
	m_rifleMesh.draw();
	

	//Binds Bunny Shader
	m_bunnyShader.bind();
	
	//Sets uniform for bunnyshader light direction
	m_bunnyShader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_bunnyTransform)));
	m_bunnyShader.bindUniform("ModelMatrix", m_bunnyTransform);
	m_bunnyShader.bindUniform("lightDirection", m_light5.direction);


	//PVM calc for bunny
	pvm = cam->getProjection() * cam->getView() * m_bunnyTransform;
	m_bunnyShader.bindUniform("ProjectionViewModel", pvm);

	//Draw Bunny Mesh
	m_bunnyMesh.draw();

	//Sets uniform for bunnyshader light direction
	m_bunnyShader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_BuddhaTransform)));
	m_bunnyShader.bindUniform("ModelMatrix", m_BuddhaTransform);
	m_bunnyShader.bindUniform("lightDirection", m_light4.direction);

	//PVM calc for Buddha
	pvm = cam->getProjection() * cam->getView() * m_BuddhaTransform;
	m_bunnyShader.bindUniform("ProjectionViewModel", pvm);

	//Draw Buddha Mesh
	m_BuddhaMesh.draw();

	// draw 3D gizmos
	aie::Gizmos::draw(cam->getProjectionView());

	glfwSwapBuffers(window);
}

/**********************************************************************************************//**
 * \fn	void Application::Terminate()
 *
 * \brief	Terminates this object
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

void Application::Terminate()
{
	//Delete Gizmos
	aie::Gizmos::destroy();

	//Deletes Camera
	delete cam;

	//Clean up window and GPU linkage
	glfwTerminate();
}
