#pragma once

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <string>
#include <vector>
#include "Texture.h"

namespace aie {

// a simple triangle mesh wrapper
class OBJMesh {
public:

	// a vertex structure for obj files
	struct Vertex {
		glm::vec4 position; // added to attrib location 0
		glm::vec4 normal;	// added to attrib location 1
		glm::vec2 texcoord;	// added to attrib location 2

							// normal-mapping data
		glm::vec4 tangent;	// added to attrib location 3
	};

	// a basic material
	class Material {
	public:

		Material() : ambient(1), diffuse(1), specular(0), emissive(0), specularPower(1), opacity(1) {}
		~Material() {}

		/// <summary>
		/// The ambient
		/// </summary>
		glm::vec3 ambient;
		/// <summary>
		/// The diffuse
		/// </summary>
		glm::vec3 diffuse;
		/// <summary>
		/// The specular
		/// </summary>
		glm::vec3 specular;
		/// <summary>
		/// The emissive
		/// </summary>
		glm::vec3 emissive;

		/// <summary>
		/// The specular power
		/// </summary>
		float specularPower;
		/// <summary>
		/// The opacity
		/// </summary>
		float opacity;

		Texture diffuseTexture;				// bound slot 0
		Texture alphaTexture;				// bound slot 1
		Texture ambientTexture;				// bound slot 2
		Texture specularTexture;			// bound slot 3
		Texture specularHighlightTexture;	// bound slot 4
		Texture normalTexture;				// bound slot 5
		Texture displacementTexture;		// bound slot 6
	};

	OBJMesh() {}
	~OBJMesh();

	// will fail if a mesh has already been loaded in to this instance
	bool load(const char* filename, bool loadTextures = true, bool flipTextureV = false);

	// allow option to draw as patches for tessellation
	void draw(bool usePatches = false);

	// access to the filename that was loaded
	const std::string& getFilename() const { return m_filename; }

	// material access
	size_t getMaterialCount() const { return m_materials.size();  }
	Material& getMaterial(size_t index) { return m_materials[index];  }

private:

	void calculateTangents(std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices);

	struct MeshChunk {
		unsigned int	vao, vbo, ibo;
		unsigned int	indexCount;
		int				materialID;
	};

	/// <summary>
	/// The m filename
	/// </summary>
	std::string				m_filename;
	/// <summary>
	/// The m mesh chunks
	/// </summary>
	std::vector<MeshChunk>	m_meshChunks;
	/// <summary>
	/// The m materials
	/// </summary>
	std::vector<Material>	m_materials;
};

} // namespace aie