#include "LoadModelShader.h"

/**********************************************************************************************//**
 * \fn	LoadModelShader::LoadModelShader()
 *
 * \brief	Initializes a new instance of the <see cref="LoadModelShader"/> class.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

LoadModelShader::LoadModelShader()
{
}

/**********************************************************************************************//**
 * \fn	LoadModelShader::~LoadModelShader()
 *
 * \brief	Finalizes an instance of the <see cref="LoadModelShader"/> class.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

LoadModelShader::~LoadModelShader()
{

}

/**********************************************************************************************//**
 * \fn	int LoadModelShader::LoadShader(aie::ShaderProgram& shaderprogram, const char* vertFileLocation, const char* fragFileLocation)
 *
 * \brief	Loads the shader.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \param [in,out]	shaderprogram   	The shaderprogram.
 * \param 		  	vertFileLocation	The vert file location.
 * \param 		  	fragFileLocation	The frag file location.
 *
 * \return	The shader.
 **************************************************************************************************/

int LoadModelShader::LoadShader(aie::ShaderProgram& shaderprogram, const char* vertFileLocation, const char* fragFileLocation)
{
	//Loading Vertex Shader
	shaderprogram.loadShader(aie::eShaderStage::VERTEX,
		vertFileLocation);
	//Loading Fragment Shader
	shaderprogram.loadShader(aie::eShaderStage::FRAGMENT,
		fragFileLocation);

	//Check if Shader is linking correctly
	if (shaderprogram.link() == false) {
		printf("Shader Error: %s\n", shaderprogram.getLastError());
		return false;
	}
	return 1;
}

/**********************************************************************************************//**
 * \fn	int LoadModelShader::LoadModel(const char* objectname, aie::OBJMesh& Mesh, const char* objFile)
 *
 * \brief	Loads the model.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \param 		  	objectname	The objectname.
 * \param [in,out]	Mesh	  	The mesh.
 * \param 		  	objFile   	The object file.
 *
 * \return	The model.
 **************************************************************************************************/

int LoadModelShader::LoadModel(const char* objectname, aie::OBJMesh& Mesh, const char* objFile)
{
	if (Mesh.load(objFile, true, true) == false) {
		printf(objectname, " Mesh Error!\n");
		return false;
	}
	return 1;
}
