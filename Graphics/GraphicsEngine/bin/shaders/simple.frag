// a simple flat colour shader
#version 410
uniform vec3 Kd;
uniform vec3 lightDirection;
in vec3 vNormal;
in vec3 pos;

out vec4 FragColour;

void main() 
{

// ensure normal and light direction are normalised
vec3 N = normalize(vNormal);
vec3 L = normalize(lightDirection);

// calculate lambert term (negate light direction)
float lambertTerm = max( 0, min( 1, dot( N, -L ) ) );

FragColour = vec4(pos, 1)* lambertTerm;
}