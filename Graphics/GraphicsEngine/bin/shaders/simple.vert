// a simple shader
#version 410
layout( location = 0 ) in vec4 Position;
layout( location = 1 ) in vec4 Normal;

// we need this matrix to transform the position
uniform mat4 ModelMatrix;

// we need this matrix to transform the normal
uniform mat3 NormalMatrix;

uniform mat4 ProjectionViewModel;
out vec3 vNormal;
out vec3 pos;

void main() 
{
vNormal = NormalMatrix * Normal.xyz;
pos = (ModelMatrix * Position).xyz;
gl_Position = ProjectionViewModel * Position;
}