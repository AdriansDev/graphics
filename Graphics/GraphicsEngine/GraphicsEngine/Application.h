#pragma once
#include <iostream>
#include <chrono>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "../Dep/glCore/gl_core_4_5.h"
#include <glfw3.h>

#include "Gizmos.h"
#include "Camera.h"
#include "FlyCamera.h" 
#include "Shader.h"
#include "Mesh.h"
#include "OBJMesh.h"
#include "LoadModelShader.h"

/**********************************************************************************************//**
 * \namespace	wsp
 *
 * \brief	Time namespace.
 **************************************************************************************************/

namespace wsp
{
	using clock = std::chrono::high_resolution_clock;
	using time = std::chrono::time_point<clock>;
}

/*TIME, CLOCK, AND DURATION Aliasing*/
using ullong = unsigned long long;
using nanoseconds = std::chrono::nanoseconds;
//Expected use: seconds = nanoseconds * NANO_TO_SECONDS
static const double NANO_TO_SECONDS = 0.000000001;
//Expected use: nanoseconds = seconds * NANO_TO_SECONDS
static const ullong NANO_IN_SECONDS = 1000000000ULL;

/**********************************************************************************************//**
 * \class	Application
 *
 * \brief	An application.
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

class Application
{
public:

	/**********************************************************************************************//**
	 * \fn	Application::Application();
	 *
	 * \brief	Default constructor
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	Application();

	/**********************************************************************************************//**
	 * \fn	Application::~Application();
	 *
	 * \brief	Destructor
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	~Application();

	/**********************************************************************************************//**
	 * \fn	int Application::Initialize(const glm::ivec2& a_resolution = glm::ivec2(1440, 900), const char* a_name = "Window");
	 *
	 * \brief	Initializes this object
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 *
	 * \param	a_resolution	(Optional) The resolution.
	 * \param	a_name			(Optional) The name.
	 *
	 * \return	An int.
	 **************************************************************************************************/

	int Initialize(const glm::ivec2& a_resolution = glm::ivec2(1440, 900), const char* a_name = "Window");

	/**********************************************************************************************//**
	 * \fn	void Application::Run();
	 *
	 * \brief	Runs this object
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	void Run();

	/**********************************************************************************************//**
	 * \fn	void Application::Render();
	 *
	 * \brief	Renders this object
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	void Render();

	/**********************************************************************************************//**
	 * \fn	void Application::Terminate();
	 *
	 * \brief	Terminates this object
	 *
	 * \author	Adrian Pedrazzoli
	 * \date	25/06/2018
	 **************************************************************************************************/

	void Terminate();

	
	/** \brief	The deltatime */
	double deltatime;

protected:

	
	/** \brief	The window */
	GLFWwindow* window;
	
	/** \brief	The screens */
	GLFWmonitor** screens;

	
	/** \brief	The view */
	glm::mat4 view;
	
	/** \brief	The projection */
	glm::mat4 projection;

	/** \brief	The m clock */
	wsp::clock m_clock;
	/** \brief	The m start time */
	wsp::time m_startTime;
	/** \brief	The m current time */
	wsp::time m_currentTime;
	/** \brief	The m previous time */
	wsp::time m_previousTime;

	/** \brief	The camera */
	Camera* cam;

	/** \brief	The load shader and object */
	LoadModelShader LoadShaderandObject;


	/** \brief	The m bunny mesh */
	aie::OBJMesh m_bunnyMesh;
	/** \brief	The m bunny transform */
	glm::mat4 m_bunnyTransform;
	
	/** \brief	The m spear mesh */
	aie::OBJMesh m_spearMesh;
	/** \brief	The m spear transform */
	glm::mat4 m_spearTransform;

	/** \brief	The m buddha mesh */
	aie::OBJMesh m_BuddhaMesh;
	/** \brief	The m buddha transform */
	glm::mat4 m_BuddhaTransform;

	/** \brief	The m camera mesh */
	aie::OBJMesh m_CameraMesh;
	/** \brief	The m camera transform */
	glm::mat4 m_CameraTransform;

	/** \brief	The m rifle mesh */
	aie::OBJMesh m_rifleMesh;
	/** \brief	The m rifle transform */
	glm::mat4 m_rifleTransform;

	/** \brief	The m rifle shader */
	aie::ShaderProgram m_rifleShader;
	/** \brief	The m camera shader */
	aie::ShaderProgram m_cameraShader;
	/** \brief	The m shader */
	aie::ShaderProgram m_shader;
	/** \brief	The m phong shader */
	aie::ShaderProgram m_phongShader;
	/** \brief	The m bunny shader */
	aie::ShaderProgram m_bunnyShader;


	struct Light {
		/** \brief	The direction */
		glm::vec3 direction;
		/** \brief	The diffuse */
		glm::vec3 diffuse;
		/** \brief	The specular */
		glm::vec3 specular;
	};


	/** \brief	The m light5 */
	Light m_light5;

	/** \brief	The m light4 */
	Light m_light4;

	/** \brief	The m light3 */
	Light m_light3;

	/** \brief	The m light2 */
	Light m_light2;

	/** \brief	The m light */
	Light m_light;

	/** \brief	The m ambient light */
	glm::vec3 m_ambientLight;


};

