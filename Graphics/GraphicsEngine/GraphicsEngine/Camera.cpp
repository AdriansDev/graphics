#include "Camera.h"

/**********************************************************************************************//**
 * \fn	Camera::Camera()
 *
 * \brief	Default constructor
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

Camera::Camera()
{
	viewTransform = glm::mat4(1);
	worldTransform = glm::mat4(1);
	projectionTransform = glm::mat4(1);

}

/**********************************************************************************************//**
 * \fn	Camera::~Camera()
 *
 * \brief	Destructor
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

Camera::~Camera()
{
}

/**********************************************************************************************//**
 * \fn	void Camera::setPerspective(float fieldOfView, float aspectRatio, float near, float far)
 *
 * \brief	Sets a perspective
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \param	fieldOfView	The field of view.
 * \param	aspectRatio	The aspect ratio.
 * \param	near	   	The near.
 * \param	far		   	The far.
 **************************************************************************************************/

void Camera::setPerspective(float fieldOfView, float aspectRatio, float near, float far)
{
	// Sets Projection Transform. 
	projectionTransform = glm::perspective(glm::pi<float>() * fieldOfView, aspectRatio, near, far);

	//Update ViewProjection
	updateProjectionViewTransform();
}

/**********************************************************************************************//**
 * \fn	void Camera::setLookAt(glm::vec3 from, glm::vec3 to, glm::vec3 up)
 *
 * \brief	Sets look at
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \param	from	Source for the.
 * \param	to  	to.
 * \param	up  	The up.
 **************************************************************************************************/

void Camera::setLookAt(glm::vec3 from, glm::vec3 to, glm::vec3 up)
{
	//Sets View Transform
	viewTransform = glm::lookAt(from,to,up);
	worldTransform = glm::inverse(viewTransform);

	//Update ViewProjection
	updateProjectionViewTransform();
}

/**********************************************************************************************//**
 * \fn	void Camera::setPosition(glm::vec3 position)
 *
 * \brief	Sets a position
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \param	position	The position.
 **************************************************************************************************/

void Camera::setPosition(glm::vec3 position)
{
	//Sets position of the world transform
	worldTransform[3] = glm::vec4(position,1);

	//Inverses World Transform
	viewTransform = glm::inverse(worldTransform);

	//Update ViewProjection
	updateProjectionViewTransform();
}

/**********************************************************************************************//**
 * \fn	glm::mat4 Camera::getWorldTransform()
 *
 * \brief	Gets world transform
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \return	The world transform.
 **************************************************************************************************/

glm::mat4 Camera::getWorldTransform()
{
	//Returns World Transform
	return worldTransform;
}

/**********************************************************************************************//**
 * \fn	glm::mat4 Camera::getView()
 *
 * \brief	Gets the view
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \return	The view.
 **************************************************************************************************/

glm::mat4 Camera::getView()
{
	//Returns View Transform
	return viewTransform;
}

/**********************************************************************************************//**
 * \fn	glm::mat4 Camera::getProjection()
 *
 * \brief	Gets the projection
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \return	The projection.
 **************************************************************************************************/

glm::mat4 Camera::getProjection()
{
	//Returns Projection Transform
	return projectionTransform;
}

/**********************************************************************************************//**
 * \fn	glm::mat4 Camera::getProjectionView()
 *
 * \brief	Gets projection view
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 *
 * \return	The projection view.
 **************************************************************************************************/

glm::mat4 Camera::getProjectionView()
{
	//Multiplys Projection Transform and View Transform
	return projection_x_view;
}

/**********************************************************************************************//**
 * \fn	void Camera::updateProjectionViewTransform()
 *
 * \brief	Updates the projection view transform
 *
 * \author	Adrian Pedrazzoli
 * \date	25/06/2018
 **************************************************************************************************/

void Camera::updateProjectionViewTransform()
{
	//Update ViewProjection
	projection_x_view = projectionTransform * viewTransform;
}
